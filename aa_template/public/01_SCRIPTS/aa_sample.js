import  {forEach,forEachObject,unless,times,every,some,sortBy} from "./LIB/es6-functional.js";




function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
}

function fn()
{
    document.getElementById("TestCase").innerHTML = "This content changed from script";






    var people = [
        {firstname: "ccFirstName", lastname: "aalastName"},
        {firstname: "aaFirstName", lastname: "cclastName"},

        {firstname:"bbFirstName", lastname:"bblastName"}
    ];

    console.log(people);

    //sorting with respect to lastname
    console.log("FirstName sort manually",people.sort((a,b) => {

        return (a.lastname < b.lastname) ? -1 : (a.lastname > b.lastname) ? 1 : 0;


    }));

    console.log("#######################################################");




//sorting with respect to firstname using sortBy
    console.log("Firstname using sortBy hoc",people.sort(sortBy("firstname")));

//sorting with respect to firstname using sortBy
    console.log("lastName using sortBy hoc",people.sort(sortBy("lastname")));




}
/*



var apple = new function() {
    this.type = "macintosh";
    this.color = "red";
    this.getInfo = function () {
        return this.color + ' ' + this.type + ' apple';
    };
}




apple.color = "reddish";
alert(apple.getInfo());




 */

if(document.readyState != "loading")
{
    fn();


}
else
{
    document.addEventListener("DOMContentLoaded", fn);
}



 


//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhY2thZ2UuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFhX3NhbXBsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAge2ZvckVhY2gsZm9yRWFjaE9iamVjdCx1bmxlc3MsdGltZXMsZXZlcnksc29tZSxzb3J0Qnl9IGZyb20gXCIuL0xJQi9lczYtZnVuY3Rpb25hbC5qc1wiO1xyXG5cclxuXHJcblxyXG5cclxuZnVuY3Rpb24gc2xlZXAoZGVsYXkpIHtcclxuICAgIHZhciBzdGFydCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xyXG4gICAgd2hpbGUgKG5ldyBEYXRlKCkuZ2V0VGltZSgpIDwgc3RhcnQgKyBkZWxheSk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGZuKClcclxue1xyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJUZXN0Q2FzZVwiKS5pbm5lckhUTUwgPSBcIlRoaXMgY29udGVudCBjaGFuZ2VkIGZyb20gc2NyaXB0XCI7XHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbiAgICB2YXIgcGVvcGxlID0gW1xyXG4gICAgICAgIHtmaXJzdG5hbWU6IFwiY2NGaXJzdE5hbWVcIiwgbGFzdG5hbWU6IFwiYWFsYXN0TmFtZVwifSxcclxuICAgICAgICB7Zmlyc3RuYW1lOiBcImFhRmlyc3ROYW1lXCIsIGxhc3RuYW1lOiBcImNjbGFzdE5hbWVcIn0sXHJcblxyXG4gICAgICAgIHtmaXJzdG5hbWU6XCJiYkZpcnN0TmFtZVwiLCBsYXN0bmFtZTpcImJibGFzdE5hbWVcIn1cclxuICAgIF07XHJcblxyXG4gICAgY29uc29sZS5sb2cocGVvcGxlKTtcclxuXHJcbiAgICAvL3NvcnRpbmcgd2l0aCByZXNwZWN0IHRvIGxhc3RuYW1lXHJcbiAgICBjb25zb2xlLmxvZyhcIkZpcnN0TmFtZSBzb3J0IG1hbnVhbGx5XCIscGVvcGxlLnNvcnQoKGEsYikgPT4ge1xyXG5cclxuICAgICAgICByZXR1cm4gKGEubGFzdG5hbWUgPCBiLmxhc3RuYW1lKSA/IC0xIDogKGEubGFzdG5hbWUgPiBiLmxhc3RuYW1lKSA/IDEgOiAwO1xyXG5cclxuXHJcbiAgICB9KSk7XHJcblxyXG4gICAgY29uc29sZS5sb2coXCIjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXCIpO1xyXG5cclxuXHJcblxyXG5cclxuLy9zb3J0aW5nIHdpdGggcmVzcGVjdCB0byBmaXJzdG5hbWUgdXNpbmcgc29ydEJ5XHJcbiAgICBjb25zb2xlLmxvZyhcIkZpcnN0bmFtZSB1c2luZyBzb3J0QnkgaG9jXCIscGVvcGxlLnNvcnQoc29ydEJ5KFwiZmlyc3RuYW1lXCIpKSk7XHJcblxyXG4vL3NvcnRpbmcgd2l0aCByZXNwZWN0IHRvIGZpcnN0bmFtZSB1c2luZyBzb3J0QnlcclxuICAgIGNvbnNvbGUubG9nKFwibGFzdE5hbWUgdXNpbmcgc29ydEJ5IGhvY1wiLHBlb3BsZS5zb3J0KHNvcnRCeShcImxhc3RuYW1lXCIpKSk7XHJcblxyXG5cclxuXHJcblxyXG59XHJcbi8qXHJcblxyXG5cclxuXHJcbnZhciBhcHBsZSA9IG5ldyBmdW5jdGlvbigpIHtcclxuICAgIHRoaXMudHlwZSA9IFwibWFjaW50b3NoXCI7XHJcbiAgICB0aGlzLmNvbG9yID0gXCJyZWRcIjtcclxuICAgIHRoaXMuZ2V0SW5mbyA9IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jb2xvciArICcgJyArIHRoaXMudHlwZSArICcgYXBwbGUnO1xyXG4gICAgfTtcclxufVxyXG5cclxuXHJcblxyXG5cclxuYXBwbGUuY29sb3IgPSBcInJlZGRpc2hcIjtcclxuYWxlcnQoYXBwbGUuZ2V0SW5mbygpKTtcclxuXHJcblxyXG5cclxuXHJcbiAqL1xyXG5cclxuaWYoZG9jdW1lbnQucmVhZHlTdGF0ZSAhPSBcImxvYWRpbmdcIilcclxue1xyXG4gICAgZm4oKTtcclxuXHJcblxyXG59XHJcbmVsc2Vcclxue1xyXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZm4pO1xyXG59XHJcblxyXG5cclxuXHJcbiBcclxuXHJcbiJdfQ==
