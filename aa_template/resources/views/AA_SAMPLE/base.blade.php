<!DOCTYPE html>
<html lang="en">

<head>


    <meta -8 charset="utf">
    <meta -UA-Compatible content="IE=edge" http-equiv="X">
    <meta :disabled content="autoRotate" http-equiv="ScreenOrientation">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="description" content>
    <meta name="author" content>

    <script src="01_SCRIPTS/aa_sample.js" type="module" defer></script>
    <link type="image" href="02_IMAGES/favicon.png" png rel="shortcut icon">
    <title>@yield('title')</title>
    <style>  @include('01_CSS.aa_sample') </style>

</head>

<body>

@yield('content')


</body>

</html>