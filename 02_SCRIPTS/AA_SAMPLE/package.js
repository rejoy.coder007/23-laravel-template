import  {forEach,forEachObject,unless,times,every,some,sortBy} from "./LIB/es6-functional.js";




function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
}

function fn()
{
    document.getElementById("TestCase").innerHTML = "This content changed from script";






    var people = [
        {firstname: "ccFirstName", lastname: "aalastName"},
        {firstname: "aaFirstName", lastname: "cclastName"},

        {firstname:"bbFirstName", lastname:"bblastName"}
    ];

    console.log(people);

    //sorting with respect to lastname
    console.log("FirstName sort manually",people.sort((a,b) => {

        return (a.lastname < b.lastname) ? -1 : (a.lastname > b.lastname) ? 1 : 0;


    }));

    console.log("#######################################################");




//sorting with respect to firstname using sortBy
    console.log("Firstname using sortBy hoc",people.sort(sortBy("firstname")));

//sorting with respect to firstname using sortBy
    console.log("lastName using sortBy hoc",people.sort(sortBy("lastname")));




}
/*



var apple = new function() {
    this.type = "macintosh";
    this.color = "red";
    this.getInfo = function () {
        return this.color + ' ' + this.type + ' apple';
    };
}




apple.color = "reddish";
alert(apple.getInfo());




 */

if(document.readyState != "loading")
{
    fn();


}
else
{
    document.addEventListener("DOMContentLoaded", fn);
}



 

